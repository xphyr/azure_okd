FROM centos:7
RUN yum -y install \
    https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm && \
    sed -i -e "s/^enabled=1/enabled=0/" /etc/yum.repos.d/epel.repo && \
    yum -y --enablerepo=epel install wget git ansible python-pip pyOpenSSL && yum clean all
RUN pip install 'ansible[azure]'
RUN mkdir -p /okdinstall/setup_scripts
RUN cd /okdinstall && git clone https://github.com/openshift/openshift-ansible && \
    cd openshift-ansible && \
    git checkout release-3.11
WORKDIR /okdinstall
COPY ansible.cfg /okdinstall
COPY setup_scripts/* /okdinstall/setup_scripts/