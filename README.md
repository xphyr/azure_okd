# OKD Install for Azure compute

The following is a cheat sheet for creating an OKD (OpenShift) cluster in Azure.  The idea is to make it as seemless as possible.  It will suport "All-In-One" configurations as well as more distributed configs.

In order to make the process simple to use we will have a docker container that can be pulled and with a little work, you can run the install from a docker container.

## Building the docker container

You are welcome to use the published container or follow these steps to build
`$ docker build -t okdinstaller:latest .`

## Using the container

To leverage the container do the following:
`$ docker run -i -t -v $HOME/.ssh/id_rsa:/root/.ssh/id_rsa -v $HOME/.ssh/id_rsa.pub:/root/.ssh/id_rsa.pub -v $HOME/.azure/credentials:/root/.azure/credentials -v $(pwd)/inventory-aio:/tmp/inventory okdinstaller:latest`

Once in the container run the following playbooks:

```
$ ansible-playbook -i /tmp/inventory setup_scripts/azure_provision_hosts.yml
$ ansible-playbook -i /tmp/inventory setup_scripts/prep_hosts.yml
## At this point you need to create DNS entries in your favorite DNS app
## OR update the inventory file to use xip.io host name
$ ansible-playbook -i /tmp/inventory openshift-ansible/playbooks/prerequisites.yml
$ ansible-playbook -i /tmp/inventory openshift-ansible/playbooks/deploy_cluster.yml
# This will set the admin password and create the OKD Master admin account
# Be sure to check your inventory for the default password and change as necessary
$ ansible-playbook -i /tmp/inventory setup_scripts/secure_okd.yml
```

## Creating a Multi-host cluster

Update the inventory-multi file. Be sure to replace all instances of "\<your domain name here\>" in the inventory file with your domain name. We also need to create two additional records. Create a CNAME record "console.okd.\<your domain name here\> that points to the "okdmstr" server. Also create a wildcard A record for "*.apps.okd.\<your domain name here\> and point that to the same IP as your okdinf host.

To leverage the container do the following:
`$ docker run -i -t -v $HOME/.ssh/id_rsa:/root/.ssh/id_rsa -v $HOME/.ssh/id_rsa.pub:/root/.ssh/id_rsa.pub -v $HOME/.azure/credentials:/root/.azure/credentials -v $(pwd)/inventory-multi:/tmp/inventory okdinstaller:latest`

Once in the container run the same commands as above with one exception. After running the "azure_provision_hosts.yml" file you will need to create DNS entries for each of the hosts listed in the inventory file.

## Cleanup

If you want to delete your OKD cluster, you can do that via the use of an Ansible playbook as well. Use the same command you used to start the docker container above and then run the following command:

```bash
$ ansible-playbook -i /tmp/inventory setup_scripts/azure_cleanup.yml
```
